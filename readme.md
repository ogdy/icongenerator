# Random Icon Generator

This D3.js snippet will generate a random icon each time the page is refreshed. Results can be tweaked by changing the random ranges. See html samples.
This is what I use for all my other project icons.

Generated shapes are SVG and can be exported as images with the button included in the samples (the export script is by Nikita Rokotyan and the file exporter is by Eli Grey.)