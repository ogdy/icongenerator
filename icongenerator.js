/*
***Abstract Icon Generator***
By Olivier GX

inputs parameters :
var width = 600;
var height = 600;
var shapesMin = 3;
var shapesMax = 5;
var shapeComplexityMin = 4;
var shapeComplexityMax = 8;
var shapeCircularRepeatMin = 4;
var shapeCircularRepeatMax = 10;

(include theses parameters in your HTML file)

Done in one or two hours (mini D3.js project for training purpose).
*/


// Runtime params
var centerX = width / 2;
var centerY = height / 2;
var maxradius = (centerX + centerY) / 3;
	
// Making an SVG Container
var svg = d3.select("body").append("svg")
	.attr("width", width)
	.attr("height", height);

// Doing the fun stuff !
var nshapes = getRandomInRange(shapesMin, shapesMax);
for (j = 0; j < nshapes; j++) {

	// Creating paths
	var lineData = [{}];
	
	var npoints = getRandomInRange(shapeComplexityMin, shapeComplexityMax);
	var fpoint = {"x": 0,  "y": 0};
	lineData[0] = fpoint;
	for (k = 1; k < npoints; k++) {
		lineData[k] = {
			"x": getRandomInRange(Math.abs(k - npoints / 2), maxradius),
			"y": getRandomInRange(-maxradius, maxradius)
		};
	}
	lineData[k] = fpoint;
	
	// Generating colors
	var color = getRandomColor();
	var opacity = Math.random();
	
	// Generating and positionning shapes
	var nrepeats = getRandomInRange(shapeCircularRepeatMin, shapeCircularRepeatMax);
	var angleOffset = getRandomInRange(-90, 90);
	for (i = 0; i < nrepeats; i++) {
		var angle = Math.PI * 2 * i / nrepeats + angleOffset;
		var shape = generateShape(lineData, color, opacity);
		shape.attr('transform', 'translate(' + centerX + ',' + centerY + ')rotate(' + angle * 180 / Math.PI + ')')
	}
}

// Generates a random shape based on a seed value
function generateShape(lineData, color, opacity){

	var lineFunction = d3.svg.line()
		.x(function(d) { return d.x; })
		.y(function(d) { return d.y; })
		.interpolate('linear');
  
	return svg.append("path")
		.attr("d", lineFunction(lineData))
		//.attr("stroke", "blue")
		//.attr("stroke-width", 2)
		.attr("opacity", opacity)
		.attr("fill", color);
}

// Returns a random int in the specified range
function getRandomInRange(min, max){
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Returns a random color
function getRandomColor() {
	var letters = '0123456789ABCDEF';
	var color = '#';
	for (var i = 0; i < 6; i++ ) {
		color += letters[Math.floor(Math.random() * 16)];
	}
	return color;
}